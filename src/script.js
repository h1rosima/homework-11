//1
const btn = document.getElementById('btn-click');
const section = document.createElement('section');
section.id = 'content';
document.body.append(section);

btn.addEventListener('click', function (){
    let p = document.createElement('p');
    p.textContent = "New Paragraph";
    section.append(p);
});

//2
const myFooter = document.createElement('footer');
const mySection = document.createElement('section');
document.body.append(myFooter);
myFooter.before(mySection);

const btnInput = document.createElement('button');
btnInput.id = "btn-input-create";
btnInput.style.width = '40px';
btnInput.style.height = '20px';
btnInput.style.margin = '10px';

mySection.style.display = 'flex';
mySection.style.flexDirection = 'column';
mySection.style.width = '200px';

mySection.append(btnInput);

btnInput.addEventListener('click', function () {
    const myInput = document.createElement('input');
    myInput.setAttribute('type', 'text');
    myInput.setAttribute('placeholder', 'Enter some text');
    myInput.setAttribute('name', 'myInput');
    myInput.style.margin = '5px';
    btnInput.after(myInput);
});

